batmand (0.3.2-22) UNRELEASED; urgency=medium

  * debian/control:
    - Upgraded to policy 4.6.0, no changes required
    - Switch to correct DEP-14 unstable branch name

 -- Sven Eckelmann <sven@narfation.org>  Sat, 21 Nov 2020 08:21:30 +0100

batmand (0.3.2-21) unstable; urgency=medium

  * debian/control:
    - Switch debhelper dependency to debhelper-compat
    - Upgraded to policy 4.5.0, no changes required
    - Allow build without (fake)root
    - Switch to debhelper compat 13
    - Switch maintainer to Debian CommunityWLAN Team
  * debian/batmand.postinst:
    - Drop workaround for upgrades from 0.3.2-2
  * debian/copyright
    - Update copyright years to 2020
  * Drop duplicated fields from upstream/metadata
  * Update debian/watch for format 4

 -- Sven Eckelmann <sven@narfation.org>  Sun, 12 Jul 2020 11:48:11 +0200

batmand (0.3.2-20) unstable; urgency=medium

  * debian/control:
    - Add Pre-Depends for --skip-systemd-native invoke-rc.d
    - Drop unused lsb-base dependency

 -- Sven Eckelmann <sven@narfation.org>  Sun, 28 Jul 2019 11:47:48 +0200

batmand (0.3.2-19) unstable; urgency=medium

  * debian/copyright:
    - Update copyright year
  * Update to debhelper to 12
  * debian/control:
    - Move Vcs to communitywlan-team group
  * Upgraded to policy 4.4.0, no changes required

 -- Sven Eckelmann <sven@narfation.org>  Sun, 21 Jul 2019 13:10:16 +0200

batmand (0.3.2-18) unstable; urgency=medium

  * Upgraded to policy 4.2.1
    - Switch from priority extra to optional
    - remove get-orig-source rule from debian/rules
  * Download snapshots via https:// in debian/get-orig-source.sh
  * Reference repository only over https:// in debian/upstream/metadata
  * debian/copyright:
    - Update copyright years
  * debian/control:
    - update to debhelper 10
    - Move VCS-* to salsa.debian.org
  * debian/rules
    - Remove ddeb migration conflict against pre-stretch package
    - Drop (now default) parameter --parallel for dch
    - Remove unused INSTALL variable
    - Use pkg-info.mk to add the package version as internal batmand version

 -- Sven Eckelmann <sven@narfation.org>  Thu, 18 Oct 2018 19:27:29 +0200

batmand (0.3.2-17) unstable; urgency=medium

  * Update copyright years in debian/copyright
  * Replace Holger Levsen as maintainer of batmand (Closes: #809647)
  * debian/control
    - Change Vcs-Git and Vcs-Browser to https://
  * debian/upstream/signing-key.asc
    - Import new upstream RSA 4096 key 2DE9541A85CC87D5D9836D5E0C8A47A2ABD72DF9
  * Sort debian control files with `wrap-and-sort -abst`

 -- Sven Eckelmann <sven@narfation.org>  Sun, 31 Jan 2016 18:26:22 +0100

batmand (0.3.2-16) unstable; urgency=medium

  * Upgraded to policy 3.9.6, no changes required
  * Update years in debian/copyright
  * debian/patches:
    - Move patches to alternative DEP-3 format and manage them with gbp pq
  * debian/control, debian/rules:
    - Drop batmand-dbg in favor of -dbgsym package
  * debian/batmand.service
    - Drop whitespaces around EnvironmentFile key
  * Drop unused lintian overrides

 -- Sven Eckelmann <sven@narfation.org>  Wed, 30 Dec 2015 19:52:29 +0100

batmand (0.3.2-15) unstable; urgency=low

  * debian/watch:
    - Verify new upstream versions using GPG key 96E5AF383F7C593B6B16
  * debian/copyright:
    - Update e-mail address of Simon Wunderlich
    - Update e-mail address of Marek Lindner
  * debian/patches:
    - Update email_addresses.patch, Include new mail address for Marek Lindner
      and Simon Wunderlich
    - Add nodetach.patch, Allow one to disable forking to background in debug_mode 0
    - Replace lto.patch with upstream linker-flags.patch
    - Add no_unchecked_binary_execute.patch. Don't execute unchecked binaries
    - Add hash_resize_leak.patch, Use memleak/error path free implementation
      of hash_resize
    - Add bitarry_shift.patch, Fix bitarray 1 bit shift type
    - Add setsockopt_leak.patch, Free socket when setsockopt failed
    - Add schedule_packet_null.patch, Don't try to schedule_own_packet with
      no if_incoming
    - Add dereference_before_check.patch, Don't dereference orig_node before
      checking for NULL
  * debian/control:
    - Upgraded to policy 3.9.5, no changes required
    - Depend on lsb-base for the initscript LSB functions
  * debian/batmand.init
    - Rewrite based on initscripts 2.88dsf-52 skeleton
  * Add systemd service unit configuration batmand.service
  * Update copyright years in debian/copyright
  * debian/rules:
    - Use Largefile Support enabled C API
    - Move LTO setting to DEB_CFLAGS_MAINT_APPEND

 -- Sven Eckelmann <sven@narfation.org>  Fri, 01 Aug 2014 23:10:26 +0200

batmand (0.3.2-14) unstable; urgency=low

  * Fix Email address of Andreas Langer in debian/copyright
  * debian/patches:
    - Add dead_links.patch, Fix dead links in documentation
    - Add email_addresses.patch, Fix Email addresses as requested by
      Andreas Langer

 -- Sven Eckelmann <sven@narfation.org>  Tue, 07 May 2013 08:18:59 +0200

batmand (0.3.2-13) unstable; urgency=low

  * Update debian/copyright
  * Upgraded to policy 3.9.4, no changes required
  * Remove obsolete DM-Upload-Allowed in debian/control
  * debian/patches:
    - Add define_gnu_source.patch, Define _GNU_SOURCE for all POSIX target
      source files (Closes: #703540)
    - Add lto.patch, Enable Link-time optimization for smaller binaries
    - Add strict-aliasing.patch, Disable strict-aliasing to avoid triggering
      aliasing problems
  * debian/rules:
    - Enable link-time optimization
    - Enable garbage collection of sections

 -- Sven Eckelmann <sven@narfation.org>  Sun, 05 May 2013 12:40:58 +0200

batmand (0.3.2-12) unstable; urgency=low

  * Update URL in debian/watch
  * Update copyright years in debian/copyright
  * Enable all hardening flags in debian/rules
  * Upgraded to policy 3.9.3, no changes required
  * Upgrade debhelper compat to v9
  * Let debhelper set the buildflags implicitly

 -- Sven Eckelmann <sven@narfation.org>  Sat, 28 Apr 2012 11:38:43 +0200

batmand (0.3.2-11) unstable; urgency=low

  * Update Vcs-* fields to new anonscm.debian.org URLs in debian/control
  * Mark all targets in debian/rules as phony
  * Remove hardening-includes which are now integrated in dpkg-
    buildflags
  * Use debian packaging manual URL as format identifier in
    debian/copyright
  * Let dh_installinit handle the installation of /etc/default/batmand
  * debian/patches:
    - Add non_parallel_build.patch, Don't start parallel build for
      environments disallowing it
    - Add build_rules.patch, Use make like build rules to support all
      flags provided through dpkg-buildflags
    - Add install_manpage.patch, Directly install manpage by build script
    - Add version_info.patch, Allow one to add debian revision to the version
      string

 -- Sven Eckelmann <sven@narfation.org>  Sun, 11 Dec 2011 14:09:48 +0100

batmand (0.3.2-10) unstable; urgency=low

  * debian/patches:
    - Add ftbfs_secondbuild.patch, Fix FTBFS on second build attempt
    - Add man_spelling_errors.patch, Fix spelling errors in manpage
    - Remove unnecessary netdevice_ops.patch, nonblock_ioctl.patch,
      outoftree_kbuild.patch
  * Upgraded to policy 3.9.2, no changes required
  * Remove support for the gateway kernel module due to missing upstream
    maintenance
    - Eliminate batmand-gateway-source and batmand-gateway-dkms from
      debian/control
    - Delete source preparation from debian/rules
    - Remove module-assistent input files control.modules.in and
      postinst.modules.in
    - Remove dkms script batmand-gateway-dkms.dkms
  * Switch to dh for debian/rules
  * Fix paths in lintian init.d-script-possible-missing-stop override

 -- Sven Eckelmann <sven@narfation.org>  Fri, 20 May 2011 21:06:16 +0200

batmand (0.3.2-9) unstable; urgency=low

  * Upload to unstable
  * Keep dependencies on separate lines in debian/control
  * debian/copyright:
    - Update to DEP5 revision 164
    - Update copyright years

 -- Sven Eckelmann <sven@narfation.org>  Sun, 06 Feb 2011 11:46:58 +0100

batmand (0.3.2-8) experimental; urgency=low

  * Updated my maintainer e-mail address
  * debian/patches:
    - Add nonblock_ioctl.patch, Use non-blocking ioctl to fix FTBFS with linux
      >= 2.6.35 (Closes LP: #690722)

 -- Sven Eckelmann <sven@narfation.org>  Wed, 15 Dec 2010 21:35:10 +0100

batmand (0.3.2-7) experimental; urgency=low

  * Upgraded to policy 3.9.1, no changes required
  * Upgrade debhelper compat to v8
  * set *FLAGS using dpkg-buildflags in debian/rules to work like
    dpkg-buildpackage when called directly
  * debian/control:
    - Correct unusual spacing in Maintainer field
    - Use hardening-includes for CFLAGS and LDFLAGS hardened builds
  * Change upstream domain to open-mesh.org

 -- Sven Eckelmann <sven@narfation.org>  Thu, 23 Sep 2010 13:25:00 +0200

batmand (0.3.2-6) unstable; urgency=low

  * debian/copyright: Update copyright years
  * Remove outdated README.source
  * Convert to 3.0 (quilt) source format
  * Use dh_dkms to provide DKMS support
  * debian/control:
    - Upgraded to policy 3.9.0, no changes required
    - Build only on linux due dependencies on the linux routing code

 -- Sven Eckelmann <sven@narfation.org>  Mon, 28 Jun 2010 22:43:59 +0200

batmand (0.3.2-5) unstable; urgency=low

  * Remove shlibs:Depends for binary packages without shared libs dependencies
  * Add missing ${misc:Depends} debhelper for batmand-gateway-source
  * Use dkms postinst script for batmand-gateway-dkms (Closes LP: #497149)
  * Correct spelling errors found by lintian

 -- Sven Eckelmann <sven@narfation.org>  Wed, 13 Jan 2010 23:32:26 +0100

batmand (0.3.2-4) unstable; urgency=low

  * Correct description of debian/patches/netdevice_ops.patch
  * Reformat debian/control
  * Provide batmand-gateway-dkms package that uses DKMS to dynamically build
    modules for the local kernel
  * Recommend either batmand-gateway-dkms or batmand-gateway-modules for batmand
    in debian/control

 -- Sven Eckelmann <sven@narfation.org>  Tue, 20 Oct 2009 17:19:15 +0200

batmand (0.3.2-3) unstable; urgency=low

  [ Sven Eckelmann ]
  * debian/patches:
    - Change to dep3 patch tagging guidelines
    - Remove number before patches as order is given by debian/patches/series
  * Remove inactive maintainer Wesley Tsai from Uploaders in debian/control
  * Override Lintian warning init.d-script-possible-missing-stop as sendsigs
    will kill the daemon more efficiently

  [ Petter Reinholdtsen ]
  * Correct runlevels and dependencies in init.d LSB header (Closes: #548306)
  * Add batmand.postinst to recover from incorrect init.d script headers in
    previous versions

 -- Sven Eckelmann <sven@narfation.org>  Sat, 03 Oct 2009 17:49:25 +0200

batmand (0.3.2-2) unstable; urgency=low

  * Upgraded to policy 3.8.3, no changes required
  * debian/patches:
    - Add 101-netdevice_ops.patch, fix compilation against linux-2.6.31
      (Closes: #534629)
  * Recommend batmand-gateway-modules and only suggest batmand-gateway-source
  * Add README.source with information about patch management

 -- Sven Eckelmann <sven@narfation.org>  Fri, 11 Sep 2009 01:06:25 +0200

batmand (0.3.2-1) unstable; urgency=low

  * New Upstream Version
  * Remove upstream merged batmand manpage
  * Remove olsr references from /etc/default/batmand
  * debian/patches:
    - Update 100-outoftree_kbuild.patch, only activate linux26 type of
      module building
  * debian/rules:
    - Use the kernel makefiles directly to build the module to match
      linux-modules-extra-2.6 way of building it
  * debian/control:
    - Add bzip2 Depend-Indep for batmand-gateway-source
    - Allow Debian Maintainers to upload new versions of batmand
  * Convert debian/copyright to new dep5 version

 -- Sven Eckelmann <sven@narfation.org>  Sat, 13 Jun 2009 08:49:10 +0200

batmand (0.3.1-3) unstable; urgency=low

  * Upgraded to policy 3.8.1, no changes required
  * Move batmand-gateway-* to new section kernel
  * Add package batmand-dbg with debug symbols
  * debian/control: Move Vcs-* to git.debian.org
  * Install kernel module source tarball to batmand-gateway.tar.bz2 instead of
    batmand-gateway-source.tar.bz2
  * Add quilt as patch system to debian/rules
  * debian/patches:
    - Add 100-outoftree-kbuild.patch - Allow out of tree module build for
      linux-modules-extra-2.6

 -- Sven Eckelmann <sven@narfation.org>  Fri, 10 Apr 2009 00:47:07 +0200

batmand (0.3.1-2) unstable; urgency=low

  * debian/copyright:
    - Correctly state that batmand is available under version 2 of the GPL
      only. Thanks to Frank Lichtenheld <ftpmaster@debian.org> for spotting
      this while reviewing NEW.

 -- Holger Levsen <holger@debian.org>  Thu, 26 Mar 2009 17:57:54 +0100

batmand (0.3.1-1) unstable; urgency=low

  [ Sven Eckelmann ]
  * New upstream release.
  * Generate package for B.A.T.M.A.N. gateway linux kernel module
  * debian/rules:
    - Let dh_clean (>= 7) delete top level *-stamp files
    - Replace deprecated dh_clean -k with dh_prep
    - Remove unused configure target
    - Run debhelper in binary-arch only for arch dependent targets
    - Use CFLAGS from dpkg-buildpackage
  * debian/control:
    - Correct Upstream URL
    - Add myself as uploader
    - Recommend batmand by batmand-gateway-(source|modules)
  * debian/copyright:
    - Convert copyright to new machine readable copyright format
    - Update copyright years
    - Remove list.h as special copyright case due to removal
    - Add new copyright holders Antoine van Gelder and myself
  * Correct URLs to documents in upstream
  * Correct URL in debian/watch to match newer releases

  [ Holger Levsen ]
  * Gladly accept the patches by Sven :-)
  * debian/control: batmand-gateway-modules can only be a suggests,
    -source is recommended.
  * debian/copyright: add small paragraphs about the rights granted by the GPL.

 -- Holger Levsen <holger@debian.org>  Sat, 14 Mar 2009 17:04:43 +0100

batmand (0.3-3) unstable; urgency=low

  * Replace looping maintainer address batmand@packages.d.o with mine.

 -- Holger Levsen <holger@debian.org>  Sat, 29 Nov 2008 18:54:54 +0000

batmand (0.3-2) unstable; urgency=low

  * The thanks to Sven Eckelmann release! (All patches in this upload where
    provided by him, I just wrote this changelog :-)
  * Bump standards version to 3.8.0, no changes needed, added breaks to
    Uploaders: anyway. (Closes: #497158)
  * batmand.8: turn minus signs into hyphens, or escape them properly.
    (Closes: #497159)
  * Add a watch file. (Closes: #497157)

 -- Holger Levsen <holger@debian.org>  Sat, 25 Oct 2008 15:11:56 +0200

batmand (0.3-1) unstable; urgency=low

  * New upstream version.
  * Remove debian/patches and build-dependency on dpatch
  * Change maintainer email address.

 -- Holger Levsen <holger@debian.org>  Fri, 23 May 2008 15:44:24 +0200

batmand (0.3~r875-1) experimental; urgency=low

  * new upstream version (current svn from stable development branch)
  * upgraded to policy 3.7.3, no changes requiered
  * add description to 01_makefile.dpatch

 -- Holger Levsen <holger@debian.org>  Wed, 26 Dec 2007 17:02:18 +0000

batmand (0.2-2) unstable; urgency=low

  * debian/copyright: fixed the link for batmands licence pointing to the full
    text of the GPL2
  * debian/copyright: relicenced the packaging to be GPL2 or (at your option)
    any later version - Wesleys agreement to this per
    <6f0dd0c00711252021t66b848b5i57078175aee5c248@mail.gmail.com>

 -- Holger Levsen <holger@debian.org>  Mon, 26 Nov 2007 09:10:49 +0000

batmand (0.2-1) unstable; urgency=low

  [ Wesley Tsai ]
  * Initial release (Closes: #405588)

  [ Holger Levsen ]
  * put Wesley and myself into uploaders and batmand@packages.qa.d. as
    maintainer
  * added Vcs-* and Homepage: headers to debian/control
  * debian/rules: removed unused dh_ examples, don't ignore make clean error
  * debian/copyright: mention list.h explicitly

 -- Holger Levsen <holger@debian.org>  Fri, 23 Nov 2007 23:38:28 +0000
